Paragonik is a web application to store photos of receipts.
Using the paragonik you can quickly check whether your product is still under warranty.
Project website: http://kamilradziejewsk.usermd.net/

In this project I use:
- python 2.7
- django 1.8
- mysql
- bootstrap
- html
- css
- less
- jquery