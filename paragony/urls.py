"""paragony URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url, patterns
from django.contrib import admin
from django.conf import settings

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'paragony.views.homepage'),

    url(r'^accounts/create_user/$', 'paragony.views.create_user'),
    url(r'^accounts/login/$', 'paragony.views.login'),
    url(r'^accounts/auth/$', 'paragony.views.auth_view'),
    url(r'^accounts/logout/$', 'paragony.views.logout'),

    url(r'^accounts/loggedin/settings/$', 'paragony.views.settings'),
    url(r'^accounts/loggedin/delete_account/$', 'paragony.views.delete_account'),
    url(r'^accounts/loggedin/$', 'receipts.views.loggedin'),
    url(r'^accounts/loggedin/add_item/$', 'receipts.views.add_item'),
    url(r'^accounts/loggedin/edit/(?P<item_id>\d+)/$', 'receipts.views.edit_item'),
    url(r'^accounts/loggedin/delete_item/(?P<item_id>\d+)/$', 'receipts.views.delete_item'),
    url(r'^accounts/loggedin/delete_image/(?P<item_id>\d+)/(?P<img_type>[\w\-]+)/$', 'receipts.views.delete_image'),
]

if settings.DEBUG:
    urlpatterns += patterns('',
        (r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    )
