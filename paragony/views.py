#-*- coding: utf-8 -*-
from django.shortcuts import render_to_response, render
from django.http import HttpResponseRedirect
from django.contrib import auth
from django.contrib.auth.models import User
from django.core.context_processors import csrf
from django.contrib.auth.forms import UserCreationForm
from receipts.views import delete_item
from receipts.models import Items


def homepage(request):
    return render_to_response('index.html')


# rejestracja
def create_user(request):
    try:
        args = {}
        args.update(csrf(request))
        if request.method == 'POST':
            args['message'] = "Rejestracja zakończona niepowodzeniem"
            args['type_message'] = "negative"
            form = UserCreationForm(request.POST)
            if form.is_valid():
                form.save()
                args['message'] = "Rejestracja zakończona powodzeniem"
                args['type_message'] = "positive"
                return render_to_response('login.html', args)

        return render_to_response('create_user.html', args)
    except Exception as e:
        print e


# logowanie
def login(request):
    try:
        c = {}
        c.update(csrf(request))
        return render_to_response('login.html', c)
    except Exception as e:
        print e


# autoryzacja uzytkownika
def auth_view(request):
    try:
        if request.method == 'POST':
            user = request.POST.get('username', '')
            passw = request.POST.get('password', '')
            user = auth.authenticate(username=user, password=passw)

            if user is not None:
                auth.login(request, user)
                return HttpResponseRedirect('/accounts/loggedin/')
            else:
                args = {}
                args.update(csrf(request))
                args['message'] = "Nieprawidłowy login lub hasło, spróbuj jeszcze raz"
                args['type_message'] = "negative"
                return render_to_response('login.html', args)
    except Exception as e:
        print e


# Wylogowanie
def logout(request):
    try:
        auth.logout(request)
        args = {}
        args['message'] = "Zostałeś wylogowany"
        args['type_message'] = "positive"
        args.update(csrf(request))
        return render_to_response('login.html', args)
    except Exception as e:
        print e


# Ustawienia
def settings(request):
    try:
        args = {}
        if request.method == 'POST':
            password1 = request.POST.get('password1', '')
            password2 = request.POST.get('password2', '')
            if password1 == password2:
                u = User.objects.get(username__exact=request.user.username)
                u.set_password(password1)
                u.save()

                args['message'] = "Twoje hasło zostało zmienione. Zaloguj się ponownie używająć nowego hasła."
                args['type_message'] = "positive"
                return render(request, 'login.html', args)
            else:
                args['message'] = "Zmiana hasła zakończona niepowodzeniem"
                args['type_message'] = "negative"

        args.update(csrf(request))
        return render(request, 'settings.html', args)
    except Exception as e:
        print e


# Usuwanie konta
def delete_account(request):
    try:
        if request.method == 'POST':
            check = request.POST.get('check', '')
            if check == 'delete':
                user = request.user
                items = Items.objects.filter(user=user)
                for item in items:
                    delete_item(request, item.id, return_loggedin=False)

                user.delete()
                return HttpResponseRedirect('/accounts/login/')
            else:
                args = {}
                args['message'] = "Usunięcie konta zakończone niepowodzeniem. Najpierw zaznacz check."
                args['type_message'] = "negative"
                return render(request, 'settings.html', args)
        else:
            return HttpResponseRedirect('/accounts/loggedin/settings/')
    except Exception as e:
        print e
