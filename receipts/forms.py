#-*- coding: utf-8 -*-
from django import forms
from models import Items

class ItemsForm(forms.ModelForm):
    name = forms.CharField(widget=forms.TextInput(attrs={"class": "required form-control", "minlength": "2"}), label="Nazwa produktu")
    warranty = forms.CharField(widget=forms.DateInput(attrs={"class": "required dateISO form-control warranty_input", "readonly": ""}), label="Koniec gwarancji")
    category = forms.ChoiceField(choices=(Items.CATEGORY), widget=forms.Select(attrs={'class': 'required form-control category_input'}), label="Kategoria")
    img_item = forms.FileField(required=False, label="Zdjęcie produktu")
    img_receipt = forms.FileField(required=False, label="Zdjęcie paragonu")

    class Meta:
        model = Items
        fields = ('name', 'warranty', 'category', 'img_item', 'img_receipt')




