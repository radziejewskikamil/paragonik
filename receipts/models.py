#-*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User

class Items(models.Model):
    CATEGORY = (
        ('', 'Wybierz'),
        ('moda', 'Moda'),
        ('elektronika', 'Elektronika'),
        ('dom_ogrod', 'Dom & Ogród'),
        ('zdrowie_uroda', 'Zdrowie & Uroda'),
        ('motoryzacja', 'Motoryzacja'),
        ('sport', 'Sport'),
        ('rozrywka', 'Rozrywka'),
        ('bizuteria', 'Biżuteria'),
        ('narzedzia', 'Narzędzia'),
        ('inne', 'Inne')
    )

    name = models.CharField(max_length=150)
    warranty = models.DateField()
    category = models.CharField(choices=CATEGORY, max_length=40)
    img_item = models.FileField(upload_to="images/")
    img_receipt = models.FileField(upload_to="images/")
    user = models.ForeignKey(User)

    def __unicode__(self):
        return self.name
