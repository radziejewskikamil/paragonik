#-*- coding: utf-8 -*-
from django.shortcuts import render_to_response, render
from django.http import HttpResponseRedirect
from django.template import RequestContext
from django.core.context_processors import csrf
import copy
from receipts.models import Items
from forms import ItemsForm
from sorethumb.djangothumbnail import DjangoThumbnail
from sorethumb.filters.defaultfilters import ThumbnailFilter, FixedWidthFilter
from sorethumb.templatetags import sorethumb
import os
from django.conf import settings


class SmallThumb(DjangoThumbnail):
    try:
        name = 'item_thumb'
        quality = 100
        filters = [ThumbnailFilter(220, 220),
                   FixedWidthFilter(220, no_upscale=False)]
    except Exception as e:
        print e


def loggedin(request):
    try:
        item_form = ItemsForm()
        args = {}
        args.update(csrf(request))
        args['item_form'] = item_form
        if request.user.is_authenticated():
            args['user'] = request.user

            if request.GET.get('sort', '') == 'old':
                args['items'] = Items.objects.filter(user=args['user']).order_by('-id')
            elif request.GET.get('sort', '') == 'warranty':
                args['items'] = Items.objects.filter(user=args['user']).order_by('warranty')
            else:
                args['items'] = Items.objects.filter(user=args['user'])

        return render_to_response('loggedin.html', args)
    except Exception as e:
        print e


def add_item(request):
    try:
        if request.method == 'POST':
            item_form = ItemsForm(request.POST, request.FILES)
            if item_form.is_valid():
                new_item = item_form.save(commit=False)
                user = request.user
                new_item.user = user
                new_item.save()
                return HttpResponseRedirect('/accounts/loggedin/')
            else:
                args = {}
                args['message'] = "Dodanie przedmiotu zakończone niepowodzeniem."
                args['type_message'] = "negative"
                args['item_form'] = item_form
                return render_to_response('edit.html', args, context_instance=RequestContext(request))
        else:
            item_form = ItemsForm()
            return render_to_response('edit.html', {'item_form': item_form}, context_instance=RequestContext(request))
    except Exception as e:
        print e


def delete_image(request, item_id, img_type, return_edit=True):
    try:
        item = Items.objects.get(id=item_id)
        owner = item.user.id
        if owner == request.user.id:
            if img_type == 'img_item':
                # MyDevil: os.remove(settings.BASE_DIR + '/public' + sorethumb.sorethumb(item.img_item, "item_thumb"))
                os.remove(settings.BASE_DIR + sorethumb.sorethumb(item.img_item, "item_thumb"))
                item.img_item.delete(save=False)

            if img_type == 'img_receipt':
                # MyDevil: os.remove(settings.BASE_DIR + '/public' + sorethumb.sorethumb(item.img_receipt, "item_thumb"))
                os.remove(settings.BASE_DIR + sorethumb.sorethumb(item.img_receipt, "item_thumb"))
                item.img_receipt.delete(save=False)

            item.save()

        if return_edit:
            return HttpResponseRedirect('/accounts/loggedin/edit/%s/' % item_id)
    except Exception as e:
        print e


def delete_item(request, item_id, return_loggedin=True):
    try:
        item = Items.objects.get(id=item_id)
        owner = item.user.id
        if owner == request.user.id:
            if item.img_item:
                delete_image(request, item_id, 'img_item', return_edit=False)
            if item.img_receipt:
                delete_image(request, item_id, 'img_receipt', return_edit=False)
            item.delete()

        if return_loggedin:
            return HttpResponseRedirect('/accounts/loggedin/')
    except Exception as e:
        print e


def edit_item(request, item_id):
    try:
        item = Items.objects.get(id=item_id)
        item_form = ItemsForm(instance=item)
        args = {}

        if request.method == 'POST':
            item2 = copy.copy(item)

            form = ItemsForm(request.POST, request.FILES, instance=item)
            if form.is_valid():
                ed_item = form.save(commit=False)

                if ed_item.img_item and item2.img_item:
                    if ed_item.img_item.url != item2.img_item.url:
                        delete_image(request, item_id, 'img_item', return_edit=False)
                if ed_item.img_receipt and item2.img_receipt:
                    if ed_item.img_receipt.url != item2.img_receipt.url:
                        delete_image(request, item_id, 'img_receipt', return_edit=False)

                ed_item.save()
                return HttpResponseRedirect('/accounts/loggedin/')
            else:
                args['message'] = "Edycja przedmiotu zakończone niepowodzeniem."
                args['type_message'] = "negative"

        owner = item.user.id
        if owner == request.user.id:
            args.update(csrf(request))
            args['item_form'] = item_form
            args['item_id'] = item_id
            return render(request, 'edit.html', args)
        else:
            return HttpResponseRedirect('/accounts/loggedin/')
    except Exception as e:
        print e
